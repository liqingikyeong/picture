#基于openresty和mongo的图片服务

提供类似于七牛图片存储以及裁剪的服务,lua 0基础,还望各位大神们多多指点。

### 依赖和参考的项目
 1. lua-resty-upload: https://github.com/openresty/lua-resty-upload

 2. openresty.org: https://github.com/openresty/openresty.org

 3. lua-resty-mongol3: https://github.com/LuaDist2/lua-resty-mongol3 (修改部分代码支持ContentType)

 4. yum install libuuid-devel

### 安装以及使用
1. nginx 配置
```conf
    worker_processes  1;
    error_log logs/error.log info;
    user root;
    events {
        worker_connections 1024;
    }
    http {
        server {
            lua_code_cache off;
            listen 8080;
            location / {
                default_type text/html;
                content_by_lua_file /yourdir/init.lua;
            }
        }
    }
```

2. mongo 配置
    手动修改下models.object里面的内容吧，以后拆出来

3. 特点
   可以预先生成一个url然后再填充内容，比如 http://domain/{bucket}/{filename},其中bucket和filename不必关心服务器中是否存在该文件

4. 使用方法
  1. 上传以及更新
     `curl -F file=@delinfo.txt http://domain/{bucket}/{file.png}`
  2. 下载
     `wget http://domain/bucket/file.png`
  3. 判断是否存在
     `curl -X HEAD http://domain/bucket/file.png`
  4. 删除文件
     `curl -X DELETE http://domain/bucket/file.png`
  5. 上述通过判断http状态码来判断服务是否正确


### 代办事项
 1. 支持OPTIONS请求
 2. 部分变量拆成配置项
 3. 支持实时图片缩放、裁剪、旋转、水印处理